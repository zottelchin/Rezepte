# Schneller Hefeteig
Von: Gudrun
Bild: https://www.kuechengoetter.de/uploads/media/960x960/01/128601-deftiger-hefeteig-0.jpg?v=1-14
Dauer:

## Zutaten
Mehl: 400g
Trockenhefe: 1 Pck.
Wasser: 200 ml
Öl: 100 ml
Salz: 1 TL

## Zubereitung
1. Alles zu einem Teig kneten.