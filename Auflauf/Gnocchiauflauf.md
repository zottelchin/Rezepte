# Gnocchi in Paprika-Tomaten-Sauce

Von: chefkoch.de

Link: https://www.chefkoch.de/rezepte/1044541209388802/Gnocchi-aus-dem-Ofen-in-Paprika-Tomaten-Sauce.html

Bild: https://img.chefkoch-cdn.de/rezepte/1044541209388802/bilder/617117/crop-600x400/gnocchi-aus-dem-ofen-in-paprika-tomaten-sauce.jpg

Dauer: 30 min

Portionen: 4

## Zutaten

Gnocchi: 500g

Olivenöl: 2 EL

Knoblauchzehe: 1

rote Paprikaschote: 1

passierte Tomaten: 500g

Frischkäse: 200g

Mozzarella: 1 Kugel

Parmesan: 100g

Salz und Pfeffer:

Oregano:

Brühe: etwas

## Zubereitung

1. Die Gnocchi nach Packungsanweisung kochen. 
2. Die Paprikaschote putzen und in einer Küchenmaschine pürieren. Die  Knoblauchzehe abziehen, pressen und in etwas Olivenöl anbraten.
3. Das Paprikapüree zufügen und kurz mitbraten.
4. Die passierten Tomaten zugeben, dann den Frischkäse unterrühren und  aufkochen lassen. Mit Salz, Pfeffer, Oregano und Brühe abschmecken.
5. Eine Auflaufform einfetten und die Gnocchi darin verteilen, die Sauce darüber geben. 
6. Den Mozzarella in Scheiben schneiden, den Parmesan reiben und beides obendrauf verteilen. 
7. Im vorgeheizten Backofen bei 180 °C ca. 15 Minuten überbacken.