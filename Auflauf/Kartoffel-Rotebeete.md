# Kartoffel-Rote Bete-Auflauf

Von: chefkoch.de

Link: https://www.chefkoch.de/rezepte/182281078666714/Kartoffel-Rote-Bete-Auflauf.html

Bild: https://img.chefkoch-cdn.de/rezepte/182281078666714/bilder/661533/crop-600x400/kartoffel-rote-bete-auflauf.jpg

Dauer: 30 min

Portionen: 4

## Zutaten

Kartoffeln: 750g

Rote Bete: 750g

Zwiebel: 1

Ei: 1

Schmand: 1 Becher (200g)

Milch: 200g

Salz und Pfeffer:

Currypulver: 2 TL

Kreuzkümmel: 1 TL

Petersilie:

Gouda: 100g

## Zubereitung

1. Kartoffeln und Rote Bete getrennt vorgaren, etwas abkühlen lassen, schälen und in Scheiben schneiden. 
2. Zwiebel in Ringe schneiden. 
3. Alles lagenweise in eine gefettete Auflaufform füllen. Jede Schicht mit Salz und Pfeffer bestreuen.
4. Schmand mit Milch cremig rühren, mit Gewürzen, gehackter Petersilie und  Ei gut verquirlen und darüber gießen. Zuletzt mit Käse bestreuen.
5. Backzeit: 30 Min. bei 200 Grad Ober-/Unterhitze auf der mittleren Schiene.