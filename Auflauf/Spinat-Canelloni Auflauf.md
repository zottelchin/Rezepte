# Spinat-Canelloni Auflauf

Portionen: 4

Bild: /static/images/Canelloni.jpeg

Dauer: 3 Stunden

## Zutaten

Blattspinat, frisch: 400g

Zwiebeln: 2

Knoblauchzehen: 2

Ricotta: 200g

Parmesan: 50g

Basilikum: 5 Blätter

Mozzarella: 120g

Canelloni: ~3/4 Paket

stückige Tomaten: 800g

Thühringer Mett: 150g

Olivenöl:

Salz:

Pfeffer:

Muskatnuss: eine Priese

italienische Kräuter:

Zimt: eine Priese

## Zubereitung

1. Eine Zwiebel und Knoblauchzehe fein Würfeln. Den Spinat waschen und grob hacken. Zwiebeln und Knoblauch dünsten und den Spinat dazu gegben und dünsten.
2. Ricotta, Parmesan, Basilikum und Gewürze dazugeben und noch einen Moment auf der heißen Platte stehen lassen.
3. Eine Auflaufform mit Olivenöl einfetten.
4. Die Canelloni mit der Spinatmasse füllen und in die Auflaufform legen.
5. Die zweite Zwiebel und Knoblauchzehe fein würfeln und mit dem Mett anbraten. Wenn das Mett durch ist, die stückigen Tomaten dazu geben. Mit Salz, Pfeffer, einer Priese Zimt und italienischen Kräutern würzen
6. Die Bolognesesauce über die Canelloni geben, diese sollten komplett bedeckt sein.
7. Den Mozzarella in dünne Scheiben schneiden und über den Auflauf verteilen.
8. Für eine halbe Stunde bei 170°C (Umluft) in den Ofen schieben.