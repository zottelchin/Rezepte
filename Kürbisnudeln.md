# Pasta mit Kürbis
Von: Emmi kocht einfach
Bild: https://emmikochteinfach.de/wp-content/uploads/2017/10/Herbstliche-Spaghetti-mit-Kuerbis-2-1170x780.jpg
Dauer: 35min
Portionen: 4
Link: https://emmikochteinfach.de/pasta-mit-kuerbis-schnell-und-einfach

## Zutaten
Spaghetti: 400g
Hokkaido-Kürbis: 1000g
getrocknete Tomaten in Öl: ca. 150g
Walnüsse: 80g
Knoblauchzehen: 2
Chilischote: 1
Tomatenmark: 2 EL
Olivenöl: 5 EL
Parmesan: 80g
Petersilie: 30g

## Zubereitung
1. Den Ofena auf 180°C (Ober-Unterhitze) vorheitzen.
2. Den Hokkaido-Kürbis sehr gut abwaschen, entkernen, in mundgerechte Würfel schneiden und in einer passenden Schüssel mit 2 EL Olivenöl vermengen sowie nach Belieben mit einer guten Prise Meersalz und Pfeffer würzen. 
3. Die Hokkaido-Stückchen auf ein mit Backpapier ausgelegtem Backblech für ca. 15-20 Minuten auf der mittleren Schiene in den vorgeheizten Ofen geben. Die Backzeit hängt von der Größe der Würfel ab. 
4. Jetzt kannst Du das Nudelwasser aufsetzen und die Spaghetti nach Packungsanweisung bissfest garen. _BITTE BEACHTE: die Spaghetti werden tropfnass direkt aus dem Topf in die Pfanne gegeben. Im Zweifel eine Minute kürzer garen, dann kannst Du sie noch ohne weiteres im Wasser "parken" , denn sie garen ja immer etwas nach._
5. Die Chilischote erst entkernen und dann wie den Knoblauch fein hacken sowie die in Öl eingelegten getrockneten Tomaten direkt aus dem Glas nehmen (leicht abtropfen lassen) und würfeln. 
6. Außerdem den Parmesan fein reiben, die Walnüsse grob hacken sowie die Petersilie waschen, trocknen und fein hacken.
7. In einer großen beschichteten Pfanne (da müssen auch die 400g gekochten Spaghetti rein passen) erhitzt Du nun auf mittlerer Stufe 3 EL Olivenöl und lässt die Chilischote, den Knoblauch und die getrockneten Tomaten darin für ca. 2-3 Minuten anschwitzen. 
8. Kurz danach gibst Du die 2 EL Tomatenmark dazu und lässt es mit andünsten. 
9. Die Spaghetti nimmst Du, wenn sie fertig sind, mit einer Nudelzange direkt und tropfnass aus dem Kochwasser und gibst sie in die Pfanne. 
10. Die Kürbis-Würfel, die gehackten Walnüsse, den Parmesan und die Petersilie zu den Spaghetti in die Pfanne geben. 
11. Dann alles unter die Spaghetti heben bzw. miteinander vermengen und mit Meersalz und schwarzem Pfeffer aus der Mühle nach Belieben abschmecken. 