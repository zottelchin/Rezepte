# Brownies

Von: einfachbacken.de

Link: https://www.einfachbacken.de/rezepte/schoko-brownies-so-werden-sie-besonders-saftig?portions=2

Portionen: 1 Blech

Bild: https://www.einfachbacken.de/sites/einfachbacken.de/files/styles/1500_1130/public/2018-07/brownies-1_zuschnitt_bearb.png?h=7cece4cc&itok=bMtuUhez

Dauer: 45 min

## Zutaten

Zartbitterschokolade (Block Schokolade): 400g

Butter: 250g

Vanillezucker: 2 Pck.

Eier: 6

brauner Zucker: 400g

Mehl: 300g

Backpulver: 2 TL

Salz: 2 Prisen

Kakaopulver: 2 EL

## Zubereitung

1. Etwa 300g der Zartbitterschokolade mit der Butter in einem kleinen Topf schmelzen. Etwas auskühlen lassen.
2. Den Ofen auf 175 Grad (Umluft 155 Grad) vorheizen und ein Backblech mit Backpapier auslegen.
3. Eier mit Vanillezucker und braunem Zucker schaumig schlagen. Schokomasse hinzugeben. Mehl mit Backpulver, Salz und Kakaopulver  vermengen und mit der Eiermasse verrühren.
4. Übrige Schokolade hacken und unter die Teigmasse heben. Den Teig in das Blech geben, glattstreichen und im vorgeheizten Ofen circa 20 Minuten backen. 1 Blech ergibt ca. 12 Brownies.
