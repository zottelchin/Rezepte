# Rhabarberkuchen

Von: Mama
Bild: https://www.diamant-zucker.de/fileadmin/dam/_processed_/csm_Rhabarberkuchen_mit_Baiserdecke_15dde21de3.jpg
Dauer: 2h
Portionen: 1

## Zutaten
Eier: 4
Zucker: 230g
Mehl: 250g
Butter: 125g
Vanillezucker: 2
Rhabarber: 7 Stangen

## Zubereitung
1. Den Rharbarber waschen und in kleine Stücke schneiden.
2. Die Eier trennen.
3. Das Wiweiß mit 3 EL Zucker steif schlagen und in den Kühlschrank stellen.
4. Das Eigelb mit 100g Zucker, dem Mehl und der Butter zu einem Teig kneten.
5. Eine Springform einfetten und den Teig darin verteilen. _Wichtig: der Rand muss mit dem Boden verbunden sind und der Boden darf dicker als der Rand sein._
6. Den Rhabarber auf den Boden streuen und mit Vaniellezucker und Zucker bestreuen.
7. Den Kuchen dann für eine albe Stunde bei 150°C (Umluft) in den Ofen stellen.
8. Den Kuchen mit dem Eisschnee bestreichen.
9. Den Kuchen eine weitere halbe Stunde bei 120°C in den Ofen stellen, bis sich der Baiser goldbraun färbt.