# Käsekuchen (bodenlos)

Von: Angela Goldmann

Bild: https://img.bildderfrau.de/img/rezepte/crop216714235/9715869791-w1200-cv3_2-dc1/Kaesekuchen-mit-Griess.jpg

Dauer: 1 h

## Zutaten

Quark: 1kg

Butter: 125g

Zucker: 250g

Eier: 5

Vanillezucker: 1

Gries: 2 EL

Backpulver: 2 TL

Puddingpulver (Vanille): 1

## Zubereitung

1. Alle Zutaten, abgesehen vom Quark, verrühren.
2. Den Quark unterheben.
3. Bei 150°C (Umluft) ~50 Minuten im Backofen backen.