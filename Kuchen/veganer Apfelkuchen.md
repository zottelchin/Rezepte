# Veganier Apfelkuchen mit Zimtstreuseln

Von: chefkoch.de
Bild: https://img.chefkoch-cdn.de/rezepte/1569381264422841/bilder/766098/crop-960x540/veganer-apfelkuchen-mit-zimtstreuseln.jpg
Dauer: 2h
Portionen: 1
Link: https://www.chefkoch.de/rezepte/1569381264422841/Veganer-Apfelkuchen-mit-Zimtstreuseln.html

## Zutaten
Magarine: 205g
Mehl: 250g
Zucker: 8EL
Vanillezucker: 1 Pck.
Backpulver: 1 Msp.
Wasser: 60ml
Zimt: 2 TL
Äpfel: 750g
Apfelmuß: 2 EL

## Zubereitung
1. 200g Mehl, 2 EL Zucker, Vanillezucker und Backpulver mischen.
2. 125g Magarine, Wasser dazugeben und glatt verkneten.
3. Teil eine Stunde in Folie gewickelt kalt stellen.
4. 150g Mehl, 6 EL Zucker, Zimt und 80g Magarine zu Streuseln kneten und kalt stellen.
5. Die Äpfel vierteln und in dünne Scheiben schneiden.
6. Den Teig in eine gefettete, mit Mehl bestäubte Form geben und ausrollen. Einen etwa 5cm hohen Rand andrücken.
7. Den Boden mit einer Gabel mehrmals einstechen und die Äpfel auf dem Boden verteilen.
8. Den Apfelmuß erhitzen und die Äpfel damit bestreichen.
9. Zimtstreusel darauf verstreuen.
10. Im vorgeheizten Backofen bei 200°C (Ober-Unterhitze) auf der unteren Schiene ca. 40min backen.