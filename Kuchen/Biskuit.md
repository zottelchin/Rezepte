# Biskuit-Tortenboden

Von: Mama
Bild: https://www.gutekueche.at/img/rezept/31109/biskuitboden-fur-torte.jpg
Dauer: 1 h
Portionen: 1 Blech

## Zutaten
Eier: 6
Zucker: 200g
Vanillezucker: Ein Päckchen
Butter: 80g
Mehl: 200g

## Zubereitung
1. Die Eier schaumig schlagen.
2. Den Zucker und Vanillezucker unterrühren.
3. Die Butter in der Mikrowelle schmelzen.
4. Das Mehl darüber sieben.
5. Das Mehl unterheben.
6. Die Butter unterheben.
7. Den Teig auf dem Blech verteilen und in den auf 150°C vorgeheizten Ofen für zwanzig Minuten stellen.