# vegane Brownies

Von: Bianca Zapatka
Bild: https://biancazapatka.com/wp-content/uploads/2019/05/brownies-recipe-best-fudgy-rich-chocolate-beste-vegane-brownies-rezept-720x1008.jpg
Link: https://biancazapatka.com/de/die-besten-brownies/#recipe
Dauer: 35 min
Portionen: 1 Blech

## Zutaten
gemahlene Leinsamen: 2 EL
heißes Wasser: 6 EL
Kakaopulver: 75g
Backpulver: 1 TL
Salz: 1/2 TL
vegane Butter oder Kokosöl: 175g
Zucker: 250g
pflanzliche Milch: ca. 80ml
Schokodrops: 1 Tasse

## Zubereitung
1. Zunächst die Leinsamen-Eier vorbereiten: Hierfür die gemahlenen Leinsamen mit dem heißen Wasser verrühren und etwa 5 Minuten quellen lassen.
2. Den Ofen auf 175°C vorheizen. Eine Form mit etwas veganer Butter einfetten und dann zusätzlich mit Backpapier auslegen. (Das Einfetten sorgt dafür, dass das Backpapier besser an der Form haftet).
3. Die vegane&nbsp;Butter langsam in einem Topf schmelzen. Den Zucker und Vanille hinzufügen und gut verrühren.
4. Das Mehl, Kakao, Backpulver und Salz in eine große Schüssel sieben und vermischen. Die flüssige Buttermischung, pflanzliche Milch und Leinsamen-Eier hinzugeben und alles zu einem homogenen Teig verrühren (Aber bitte nicht zu lange Rühren). Zuletzt die halbe Menge der Schoko Drops unterrühren.
5. Den Teig in die vorbereitete Backform füllen und glatt streichen. Mit den restlichen Schoko Drops bestreuen und ca. 30 Minuten backen, oder bis zur gewünschten Konsistenz. (Je länger man die Brownies backt, umso trockener werden sie. Wenn man sie saftiger mag, sollte man sie nicht zu lange backen.)
6. Anschließend die Brownies 15 Minuten abkühlen lassen. Dann mit Hilfe des Backpapiers aus der Form heben und weitere 10 Minuten abkühlen lassen.
7. Nun kann man die Brownies schneiden&nbsp;und genießen!