# Quarkplinsen

Von: Gudrun

Portionen: 2

Bild: https://img.chefkoch-cdn.de/rezepte/1578211265302002/bilder/619676/crop-360x240/quarkplinsen.jpg

Dauer: 20 min

## Zutaten

Butter: 20g

Zucker: 20g

Salz: eine Priese

Eier: 2

Mehl: 150g

Quark: 500g

## Zubereitung

1. Alle Zutaten mit einem Mixer vermengen.
2. Pfanne mit Butter erhitzen.
3. Kleine Plinsen in der Butter braten.
4. Mit Apfelmus und Zucker servieren.
