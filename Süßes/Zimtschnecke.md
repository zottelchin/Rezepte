# Zimtschnecke

Von: einfachbacken.de

Link: https://www.einfachbacken.de/rezepte/rosinenschnecken-rezept-wie-vom-baecker

Portionen: 12 Schnecken

Bild: https://www.einfachbacken.de/sites/einfachbacken.de/files/styles/1500_1130/public/2019-08/rosinenschnecken.jpg?h=a1e1a043&itok=TZuR0HHy

Dauer: 90 min

## Zutaten

lauwarme Milch: 200 ml

Hefe: 1/2 Würfel

Mehl: 500g

Zucker: 80g

Butter: 140g

Ei: 1

Zimt: 1 TL

Rosinen: 3 EL

Puderzucker: 3 EL

Zitronensaft: 1 EL

Wasser: 1-2 TL

## Zubereitung

1. Ein Backblech mit Backpapier auslegen. 
2. Die lauwarme Milch mit der Hefe mischen.
3. Mehl,  60g Zucker, 80g Butter und Ei in  eine Schüssel geben und die Hefe-Milch hinzufügen, alles 5 Min. gut  durchkneten, bis ein geschmeidiger Teig entstanden ist. 
4. Den Teig abgedeckt 30 Min. gehen lassen. 
5. Den Hefeteig zu einem großen Rechteck  ausrollen. Die Butter mit einem Pinsel auf dem Hefeteig verteilen,  restlichen Zucker und Zimt mischen und darüber streuen, die Rosinen  gleichmäßig darauf verteilen. 
6. Den Teig von der langen Seite eng  aufrollen. Mit einem scharfen Messer 1,5 cm breite Schnecken schneiden  und auf das Backblech setzen.
7. Den Backofen auf 180 Grad (Umluft: 160 Grad) vorheizen. 
8. Die Schnecken nochmals 10 Minuten gehen lassen. 
9. Die Rosinenschnecken im vorgeheizten  Ofen ca. 15 Minuten backen. 
10. Aus Puderzucker, Zitronensaft und 1-2  TL Wasser einen Guss herstellen und die ausgekühlten Schnecken damit  dekorieren. 
