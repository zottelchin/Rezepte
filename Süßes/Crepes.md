# Crêpes
Portionen: 4
Dauer: 20 min
Portionen 3

Bild: https://images.kitchenstories.io/wagtailOriginalImages/unnamed_1/unnamed_1-large-landscape-150.jpg

## Zutaten
Eier: 4 
Mehl:  ca.250g (4 Schaufeln) 
Vanillezucker: ein Päckchen   
Milch: Ca. ½ l 
Salz: eine Priese
Zucker: 20g

## Zubereitung
1. Alles bis auf die Milch vermengen und nach und nach Milch hinzugeben, sodass ein flüssiger Teig entsteht. 
1. Den Teig dünn in einer Pfanne braten. 
1. Den Crêpe mit Belag genießen. 