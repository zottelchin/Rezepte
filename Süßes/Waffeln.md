# Sahnewaffeln
Von: Mama
Bild: https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/ras/Assets/C6A07F50-D9D1-48C3-85E4-8F918108C103/Derivates/9CB28C02-408C-4E6E-B955-F6C11A9E66EA.jpg
Dauer: 10min
Portionen: 8 Waffeln

## Zutaten
Eier: 4
Zucker: 40g
Vanillezucker: 1 Pck.
Magarine: 50g
Mehl: 250g
Sahne: 200g
Milch: 200ml

## Zubereitung
1. Alle Zutaten außer die Sahne und Milch vermengen.
2. Sahne und Milch dazugeben und glatt rühren.