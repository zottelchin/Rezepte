# Elisenlebkuchen

Von: backen.de
Link: https://backen.de/rezept/elisenlebkuchen
Bild: /static/images/Elisen.jpg
Portionen: 40
Dauer: 1h

## Zutaten
Zitronat: 100g
Orangat: 100g
Eier: 4
brauner Zucker: 150g
Vanillezucker: 2 Pck.
Lebkuchengewürz: 1 Pck. (15g)
Rum-Aroma: 2 Rö.
gemahlene Mandeln: 300g
Backpulver: 1/2 Pck.
gemahlene Haselnüsse: 200g
Backoblaten (Ø 7cm): ~45
Puderzucker: 200g
Wasser: 4 EL
Zitronensaft: 1 EL

## Zubereitung
1. Das Zitronat und Orangat sehr fein hacken.
2. Den Ofen auf 180°C (160°C Umluft) vorheizen und zwei Backbleche mit Backpapier auslegen.
3. 4 Eier in einer Rührschüssel eine Minute schaumig schlagen.
4. Den braunen Zucker sowie Vanillezucker einrieseln. Dann noch mal zwei Minuten verrühren. Auch das Rum-Aroma und das Lebkuchengewürz hinzugeben.
5. Im Rührgerät die Schneebesen gegen Knethaken oder ähnliches tauschen.
6. In einer Schüssel die gemahlenen Mandeln mit dem Backpulver mischen und dann zusammen mit den Haselnüssen, Zitronat und Orangat unter die Eiermasse rühren.
7. Je einen Esslöffel Teig auf einen Oblaten geben und mit einem nassen Messer kuppelförmig streichen.
8. Das Blech je 20 Minuten auf mittlerer Schiene backen.
9. Den Puderzucker mit Wasser und Zitrone zu Zuckerguss verrühren. _Den Guss ruhig etwas flüssiger anrühren._
10. Die Elisen mit dem Backpapier vom Blech ziehen und jede Elise einmal in den Guss tunken.
11. 2 Stunden trocknen lassen.