# Honig Senf Sauce (vegan)

Von: urkraut.ch

Link: https://urkraut.ch/rezepte/honig-senf-sauce-zum-dippen/

Bild: https://urkraut.ch/wp-content/uploads/2019/12/PAM8794_Honig-Senfsauce_Urkraut-320x214.jpg

Dauer: 10 min

Portionen: 200g

## Zutaten

pflanzlicher Naturjogurt: 110g

mittelscharfer Senf: 70g

Honig: 1,5 EL

Rapsöl: 1 TL

Apfelessig: 1/2 TL

Knoblauchzehe: 1/2

Salz und Pfeffer:

## Zubereitung

1. Knoblauchzehe schälen und pressen. 
2. Alle Zutaten vermengen und die Sauce kühl stellen. 