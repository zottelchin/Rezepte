# Vinaigrette

Bild: https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/ras/Assets/a631fe7b-037c-4d75-9d1a-78cf17b0c998/Derivates/e709a05e-113e-4422-9bcb-cae0a8bbc5cd.jpg

Dauer: 10min

Portionen: 4

## Zutaten
Schalotte: 1
Himberessig: 3 EL
Johannisbeergelee (oÄ): 2 TL
Haselnussöl: 3 EL
Olivenöl: 2 EL

## Zubereitung

1. Die Schalotte fein würfeln und mit den restlichen Zutaten vermengen.
2. Mit Salz und Pfeffer abschmecken.