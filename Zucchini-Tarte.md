# Zucchini-Tarte mit Ziegenkäse
Von: CZ
Bild: https://images.eatsmarter.de/sites/default/files/styles/1600x1200/public/zucchini-ziegenkaese-tarte-56473.jpg
Dauer: 1,25h

## Zutaten
Olivenöl: 2 EL
Crème fraîche: 200g 
Eier: 2
krümeliger Ziegenkäse: 250g
Blätterteig: 1 Packung
Zucchini in 1cm Schreiben: 2

## Zubereitung
1. Den Backofen auf 200°C (Umluft) vorheizen.
2. In einer Pfanne das Olivenöl erhitzen und die Zucchinischreiben darin etwa zehn Minuten goldbraun braten.
3. In einer Schüssel das Crème fraîche mit den Eiern verrühren. Den zerkrümelten Käse dazugeben und mit etwas Salz und Pfeffer untermengen.
4. Den Blätterteig in ein 30x20 Zentimeter große Backform legen und mit einer Gabel ein paar Mal einstechen.
5. Die Hälfte der Zucchinischeiben auf den Blätterteig legen, die die Eier-Käse-Masse und zum Schluss die restlichen Zucchinischeiben darauf verteilen.
6. Die Tarte 20 bis 40 Minuten backen, bis sie schön gebräunt ist. _Tipp: Sie schmeckt gut warm, man kann sie aber auch abkühlen lassen. Ein Salat aus Rucola und roten Zwiebeln ist eine schöne beilage_