# Focaccia

Von: Tante Lila
Portionen: 1 Blech
Dauer: ~2 Stunden
Bild: https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/ras/Assets/63D3854D-D39C-4999-9DC0-AFF9583DB3F7/Derivates/BF9507B3-F2C0-4262-8346-649A5F980789.jpg

## Zutaten
frische Hefe: 1/2 Würfel
Mehl: 1kg
Salz: 3 TL
Zucker: 1TL
Wasser (lauwarm): 600ml
Rosmarin: 3-4 Zweige
grobes Meersalz: 2 TL
Olivenöl: 5-7 EL

## Zubereitung
1. Die Hefe zerbröseln und im Wasser auflösen und in einer großen Schüssel mit Mehl, Salz und Zucker mit den Knethaken des Rührgerätes zu einem glatten Teig verkneten.
2. Den Teig zudecken und an einem warmen Ort 45 Minuten gehen lassen. (Er sollte sein Volumen verdoppeln)
3. Ein Backblech mit Bachpapier auslegen und den Teig darauf verteilen. _Tipp: Man kann auch zwei kleinere Leibe machen_
4. Den Teig nochmal 20 Minuten gehen lassen.
5. Den Rosmarin waschen, Nadeln abstreifen und hacken.
6. Mit den Fingern dicht an dicht Mulden in den Teig drücken und dann mit Rasmirn und Meersalz bestreuen und mit Öl beträufeln.
7. Im vorgeheizten Backofen bei 200°C (Umluft) 20-25 Minuten goldbraun backen.
8. Auf einem Rost abkühlen lassen.