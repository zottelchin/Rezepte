# Toskanischer Filet-Topf
Von: Inge
Bild: https://images.lecker.de/toskanischer-filettopf-F2683501,id=c4ffe571,b=lecker,w=440,h=440,cg=c.jpg
Portionen: 4

## Zutaten

Schweinefilet: 600g
Bacon: 1 Packung
Schlagsahne: 3 Becher
stückige Tomaten: 500g
Knoblauchzehen: 4

<!-- Gewürze -->
Paprikapulver:
Cayenne Pfeffer:
Chilipulver:
Salz und Pfeffer:
Rosmarin, Basilikum, Thymian:
Semmelbrösel:

## Zubereitung
1. Das Fleisch kalt abwaschen und trocken tupfen.
2. Eine Auflaufform mit Butter einfetten und den Backofen auf 180-200°C (Unter-Oberhitze) vorheitzen.
3. Das Fleisch in Medaillons teilen und scharf von allen Seiten anbraten.
4. Jedes Medaillon mit einer halbierten Scheibe Bacon umwickeln und dicht an dicht in die Auflaufform legen.
5. Den Knoblauch pellen und die Tomaten klein würfeln.
6. Schlagsahne im Topf erhitzen und Tomaten und etwas Tomatenmark einrühren.
7. Knoblauch pressen und mit Kräutern sowie den Gewürzen pikant abschmecken und kurz aufkochen. _Sollte die Soße zu hell sein, noch etwas Tomatenmark dazugeben._
8. Heiß über das Fleisch geben und ein paar Semmelbrösel darauf verteilen.
9.  Dann 40 Minuten im Ofen backen.
10. Dazu passt gut Reis.