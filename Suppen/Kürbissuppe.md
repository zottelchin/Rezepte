# Kürbiscremesuppe

Von: Thermomix

Dauer: 45min

Portionen: 4

Bild: https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/ras/Assets/DF16DC1D-CBC0-4AF4-8297-84D1595F111E/Derivates/836BA07E-4AEF-4975-A429-B764010D064A.jpg

## Zutaten

Ingwer (walnussgroß): 1 Stück

Schalotten: 50g

Butter: 20g

Kürbisfleisch: 400g

Möhren: 100g

Kartoffeln: 100g

Paprika edelsüß: 1/2 gestr. TL

Curry: 1/2 gestr. TL

Cayenne-Pfeffer: 1 Msp.

Wasser: 400g

Gewürzpaste (Thermi): 1 geh. TL

Kokosmilch: 1 Dose

Muskat: 1 Prise

## Zubereitung:

1. Ingwer und Schalotten schälen und zerkleinern (3 Sek./Stufe 5).
2. In Butter dünsten (3 Min./120°C/Stufe 2)
3. Kürbis und geschälte Kartoffeln und Möhren dazugeben und zerkleinern (5 Sek./Stufe 5).
4. Paprika, Curry, Cayenne-Pfeffer, Wasser, Gewürzpaste und eine halbe Dose Kokosmilch zugeben und garen (15 Min./100°C/Stufe 1).
5. Salz, Pfeffer, Muskat und die restliche Kokosmilch dazugeben und pürieren (45 Sek./Stufe 5-9), abschmecken und heiß servieren.