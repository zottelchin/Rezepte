# Kürbisrisotto mit Walnüssen

Von: chefkoch

Link: https://www.chefkoch.de/rezepte/2078681335941628/Kuerbisrisotto-mit-Walnuessen.html

Portionen: 4

Bild: https://img.chefkoch-cdn.de/rezepte/2078681335941628/bilder/953544/crop-600x400/kuerbisrisotto-mit-walnuessen.jpg

Dauer: 90 min

## Zutaten

Zwiebel: 1

Hokkaidokürbis: 400g

Gemüsebrühe: 600ml

Butter: 1 EL

Öl: 5 EL

Risottoreis: 150g

Weißwein: 100ml

Petersilie: 5 Stiele

Walnüsse: 2 EL

Parmesan: 50g

Salz und Pfeffer: 

## Zubereitung

1. Zwiebel würfeln. Kürbis putzen und mit einem Löffel entkernen. Das ungeschälte Fruchtfleisch 1,5 cm groß würfeln. 
2. Brühe in einem Topf erhitzen und warmhalten.
3. Butter und 1 EL Öl in einem weiteren Topf erhitzen. Zwiebel darin glasig dünsten. 
4. Kürbis und Reis zugeben und 2 Min. mitdünsten.
5. Mit Weißwein ablöschen und vollständig einkochen lassen. 
6. So viel Brühe zugeben, dass der Reis bedeckt ist. 
7. Offen bei mittlerer Hitze unter gelegentlichem Rühren 25 Min. garen.  Dabei immer etwas heiße Brühe zugießen, sobald sie vom Reis aufgesogen  ist.
8. Petersilie abzupfen und hacken. Walnüsse hacken und mit der Petersilie und 4 EL Öl mischen.
9. Risotto mit Salz und Pfeffer würzen und den Käse unterrühren. Mit dem Walnuss-Petersilie-Öl servieren.
